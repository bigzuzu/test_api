using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Npgsql;
using System;

namespace test_api
{
    public class UserLoginResult
    {
        public long UserID {get;set;}
        public string UserFioAll {get;set;}
        public string UserFioShort {get;set;}
        public int FilialID {get;set;}
        public string FilialNameAll {get;set;}
        public string FilialNameShort {get;set;}
        public int RoleID {get;set;}
        public string RoleName {get;set;}
        public int PostID {get;set;}
        public string PostName {get;set;}

        public async Task<IEnumerable<UserLoginResult>> UserLogin(string Login, string Pass, string Ip)
        {
            List<UserLoginResult> result = new List<UserLoginResult>();

            string sql = string.Format(
                "select user_id, user_fio_all, user_fio_short, filial_id, filial_name_all, filial_name_short, role_id, role_name, post_id, post_name "
                + "from usr.user_login('{0}','{1}','{2}')", Login, Pass, Ip);

            await using (var connect = await DatabaseConnect.GetConnectAsync())
            await using (var cmd = new NpgsqlCommand(sql, connect))
            await using (var reader = await cmd.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    var row = new UserLoginResult{
                        UserID = reader.GetInt64(0),
                        UserFioAll = reader.GetString(1),
                        UserFioShort = reader.GetString(2),
                        FilialID = reader.GetInt32(3),
                        FilialNameAll = reader.GetString(4),
                        FilialNameShort = reader.GetString(5),
                        RoleID = reader.GetInt32(6),
                        RoleName = reader.GetString(7),
                        PostID = reader.GetInt32(8),
                        PostName = reader.GetString(9)
                    };
                    result.Add(row);
                }
                await cmd.Connection.CloseAsync();
            }

            return result;
        }
    }


}