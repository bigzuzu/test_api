using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

namespace test_api
{
    [ApiController]
    [Produces("application/json")]
    public class UsersOfFilialController : ControllerBase
    {
        [Route("api/UsersOfFilial")]
        [HttpGet]
        public async Task<IActionResult> Get(long UserID, int FilialID)
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                var result = await new UsersOfFilialResult().UsersOfFilial(UserID, FilialID);
                return Ok(result);
            } else {
                return BadRequest();
            }
        }
    }
}