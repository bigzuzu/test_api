using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

namespace test_api
{
    [ApiController]
    [Produces("application/json")]
    public class LogoutController : ControllerBase
    {
        [Route("api/Logout")]
        [HttpPost]
        public async Task Post(long UserID)
        {
            if (Request.HttpContext.User.Identity.IsAuthenticated)
            {
                await new UserLogout().Logout(UserID);
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            }
        }
    }
}