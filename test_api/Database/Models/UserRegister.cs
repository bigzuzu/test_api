using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Npgsql;
using System;

namespace test_api
{
    public class UserRegisterResult
    {
        public bool Ok {get;set;}
        public string Msg {get;set;}

        public async Task<UserRegisterResult> UserRegister(
            string UserLogin,   //0
            string UserPass,    //1
            string FirstName,   //2
            string LastName,    //3
            string MiddleName,  //4
            DateTime DateBirth, //5
            long PhoneMob,      //6
            long PhoneHome,     //7
            string Email,       //8
            string DocSerial,   //9
            string DocNumber,   //10
            int DocTypeID,      //11
            long CurUserID,     //12
            int CurFilialID     //13
        )
        {
            var result = new UserRegisterResult{Ok = false, Msg = ""};

            var sql = string.Format(
                "select ok, msg from usr.register('{0}','{1}','{2}','{3}','{4}','{5}'::date,{6},{7},'{8}','{9}','{10}',{11},{12},{13})",
                UserLogin,
                UserPass,
                FirstName,
                LastName,
                MiddleName,
                DateBirth.ToString("yyyy-MM-dd"),
                PhoneMob,
                PhoneHome,
                Email,
                DocSerial,
                DocNumber,
                DocTypeID,
                CurUserID,
                CurFilialID
            );

            await using (var connect = await DatabaseConnect.GetConnectAsync())
            await using (var cmd = new NpgsqlCommand(sql, connect))
            await using (var reader = await cmd.ExecuteReaderAsync())
            {
                await reader.ReadAsync();
                result.Ok = reader.GetBoolean(0);
                result.Msg = reader.GetString(1);
            }
            
            return result;
        }
    }
}