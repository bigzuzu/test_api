using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Npgsql;

namespace test_api
{
	public static class DatabaseConnect
	{
		//static string cnString = "Host=localhost;Port=5432;Username=postgres;Password=9087654321;Database=test_admin";
		static string cnString = "Host=amnet.cloudns.cl;Port=5432;Username=sodpp;Password=sodpp_secret;Database=sodppdb";

		public static async Task<NpgsqlConnection> GetConnectAsync()
		{
			var connect = await Task.Run(() =>
			{
				var cn = new NpgsqlConnection(cnString);
				cn.Open();
				return cn;
			});

			return connect;
		}

		public static async Task<NpgsqlDataReader> GetReaderAsync(string Sql)
		{
			NpgsqlDataReader reader = null;

			await using (var connect = await DatabaseConnect.GetConnectAsync())
			await using (var cmd = new NpgsqlCommand(Sql, connect))
			{
				reader = await cmd.ExecuteReaderAsync();
				//await connect.CloseAsync();
			}
			return reader;
		}
	}
}