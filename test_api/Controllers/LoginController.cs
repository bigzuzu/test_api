using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace test_api
{
    [ApiController]
    [Produces("application/json")]
    public class LoginController : ControllerBase
    {
        [HttpGet]
        [Route("api/Login")]
        public async Task<IActionResult> Get(string UserLogin, string UserPass)
        {           
            if (!string.IsNullOrWhiteSpace(UserLogin) && !string.IsNullOrWhiteSpace(UserPass))
            {
                var ip = Request.HttpContext.Connection.RemoteIpAddress.ToString();
                IEnumerable<UserLoginResult> userInfo = await new UserLoginResult().UserLogin(UserLogin, UserPass, ip);
                
                if (userInfo != null && userInfo.Count() > 0)
                {
                    var userID = userInfo.First().UserID.ToString();
                    var userRoleID = userInfo.First().RoleID.ToString();

                    var claims  = new List<Claim> { new Claim(userID,userRoleID) };
                    var id = new ClaimsIdentity(
                        claims, 
                        "AppTestCookie", 
                        ClaimsIdentity.DefaultNameClaimType, 
                        ClaimsIdentity.DefaultRoleClaimType
                        );
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
                    
                    return Ok(userInfo);              
                } 
                else
                { 
                    return BadRequest();
                }
            }
            else 
            {
                return BadRequest();
            }
        }
    }
}