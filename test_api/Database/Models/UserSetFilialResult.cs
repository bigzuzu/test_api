using System;
using System.Threading.Tasks;
using Npgsql;

namespace test_api
{
    public class UserSetFilialResult
    {
        public bool Ok {get;set;}
        public string Msg {get;set;}     

        public async Task<UserSetFilialResult> SetFilial(
            long UserID,    //0
            int FilialID,   //1
            string ActualStart, //2
            int RoleID,     //3
            int PostID,     //4
            long CurUserID, //5
            int CurFilialID //6
            )
        {
            var result = new UserSetFilialResult { Ok = false };

            if (DateTimeOffset.TryParse(ActualStart, out DateTimeOffset actualStart) == false)
            {
                result.Ok = false;
                result.Msg = "Не удалось преобразовать дату";
                return result;
            }

            var sql = string.Format(
                "select ok, msg from usr.user_set_filial({0},{1},'{2}'::timestamp with time zone,null,{3},{4},{5},{6})",
                UserID,
                FilialID,
                actualStart,
                RoleID,
                PostID,
                CurUserID,
                CurFilialID
            );

            await using (var connect = await DatabaseConnect.GetConnectAsync())
            await using (var cmd = new NpgsqlCommand(sql, connect))
            await using (var reader = await cmd.ExecuteReaderAsync())
            {
                await reader.ReadAsync();
                result.Ok = reader.GetBoolean(0);
                result.Msg = reader.GetString(1);
            }
            
            return result;
        }
    }

    
}