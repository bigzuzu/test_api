using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Npgsql;

namespace test_api 
{
    public class RolesListResult
    {
        public int RoleID {get;set;}
        public string RoleName {get;set;}
        public string RoleComment {get;set;}

        public async Task<IEnumerable<RolesListResult>> Roles()
        {
            var result = new List<RolesListResult>();

            var sql = "select role_id, role_name, role_comment from usr.roles_list()";

            await using (var connect = await DatabaseConnect.GetConnectAsync())
            await using (var cmd = new NpgsqlCommand(sql, connect))
            await using (var reader = await cmd.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    var row = new RolesListResult
                    {
                        RoleID = reader.GetInt32(0),
                        RoleName = reader.GetString(1),
                        RoleComment = reader.GetString(2) ?? ""
                    };
                    result.Add(row);
                }
                await cmd.Connection.CloseAsync();
            }

            return result;
        }
    }
}