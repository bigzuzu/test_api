using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace test_api
{
    [ApiController]
    [Produces("application/json")]
    public class RolesController : ControllerBase
    {
        [Route("api/Roles")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                var result = await new RolesListResult().Roles();
                return Ok(result);
            } 
            else 
            {
                return BadRequest();
            }
        }
    }
}