using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace test_api
{
    [ApiController]
    [Produces("application/json")]
    public class UserRegisterController : ControllerBase
    {
        [HttpGet]
        [Route("api/UserRegister")]
        public async Task<IActionResult> Get(
            string UserLogin,
            string UserPass,
            string FirstName,
            string LastName,
            string MiddleName,
            DateTime DateBirth,
            long PhoneMob,
            long PhoneHome,
            string Email,
            string DocSerial,
            string DocNumber,
            int DocTypeID,
            long CurUserID,
            int CurFilialID
        )
        {
            if (Request.HttpContext.User.Identity.IsAuthenticated)
            {
                var result = await new UserRegisterResult().UserRegister(
                    UserLogin,
                    UserPass,
                    FirstName,
                    LastName,
                    MiddleName,
                    DateBirth,
                    PhoneMob,
                    PhoneHome,
                    Email,
                    DocSerial,
                    DocNumber,
                    DocTypeID,
                    CurUserID,
                    CurFilialID
                );        
                return Ok(result);
            } else
            {
                return BadRequest();
            }

            
            
        }
    }
}