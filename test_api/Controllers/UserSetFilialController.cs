using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace test_api
{
    [ApiController]
    [Produces("application/json")]
    public class UserSetFilialController : ControllerBase 
    {
        [Route("api/UserSetFilial")]
        public async Task<IActionResult> Get(
            long UserID,
            int FilialID,
            string ActualStart,
            int RoleID,
            long CurUserID,
            int CurFilialID
        )
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                var result = await new UserSetFilialResult().SetFilial(UserID, FilialID, ActualStart, RoleID, 1, CurUserID, CurFilialID);
                return Ok(result);
            }
            else 
            {
                return BadRequest();
            }
        }
    }
}