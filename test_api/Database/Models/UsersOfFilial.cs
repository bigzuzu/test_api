using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Npgsql;

namespace test_api
{
    public class UsersOfFilialResult
    {
        public long RfID {get;set;}
        public long UserID {get;set;}
        public string UserFioAll {get;set;}
        public string UserFioShort {get;set;}
        public int FilialID {get;set;}
        public string FilialNameAll {get;set;}
        public string FilialNameShort {get;set;}
        public string CityAllName {get;set;}

        public async Task<IEnumerable<UsersOfFilialResult>> UsersOfFilial(long UserID, int FilialID)
        {
            var result = new List<UsersOfFilialResult>();

            string sql = string.Format(
                "select rf_id, user_id, user_fio_all, user_fio_short, filial_id, filial_name_all, filial_name_short, city_all_name from usr.users_of_filial({0},{1})",
                UserID,
                FilialID
            );

            await using (var connect = await DatabaseConnect.GetConnectAsync())
            await using (var cmd = new NpgsqlCommand(sql, connect))
            await using (var reader = await cmd.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    var row = new UsersOfFilialResult
                    {
                        RfID = reader.GetInt64(0),
                        UserID = reader.GetInt64(1),
                        UserFioAll = reader.GetString(2),
                        UserFioShort = reader.GetString(3),
                        FilialID = reader.GetInt32(4),
                        FilialNameAll = reader.GetString(5),
                        FilialNameShort = reader.GetString(6),
                        CityAllName = reader.GetString(7)
                    };
                    result.Add(row);
                }
                await cmd.Connection.CloseAsync();
            }

            return result;
        }
    }
}