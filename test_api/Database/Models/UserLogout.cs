using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Npgsql;
using System;

namespace test_api
{
    public class UserLogout
    {
        public async Task Logout(long UserID)
        {
            List<UserLoginResult> result = new List<UserLoginResult>();

            string sql = string.Format("call usr.user_logout({0})", UserID);

            await using (var connect = await DatabaseConnect.GetConnectAsync())
            await using (var cmd = new NpgsqlCommand(sql, connect))
            await using (var reader = await cmd.ExecuteReaderAsync())
            {
                await cmd.ExecuteNonQueryAsync();
                await cmd.Connection.CloseAsync();
            }
        }
    }


}